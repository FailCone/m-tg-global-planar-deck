<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Magic: The Gathering Global Planar Deck</title>
    </head>
    <body>
   <h2 align='center'>Magic: The Gathering - Global Planar Deck <font size='1'>v.1</font></h2>

<p align="center"> 
<?php 
   $phenoms = array("planechase/spatial-merging.jpg", "planechase/interplanar-tunnel.jpg", "planechase/morphic-tide.jpg", "planechase/mutual-epiphany.jpg", "planechase/planewide-disaster.jpg", "planechase/reality-shaping.jpg", "planechase/time-distortion.jpg");
   function random_plane(){
   $directory = "planechase";
   $fileList = glob($directory . '/*.*');
   $file = array_rand($fileList);
   return $fileList[$file];
   }

   function display_plane($plane){
     echo "<img src='";
     echo $plane;
     echo "' width='600' height='420'/>";
   }

?>
<?php 
$planeToDisplay = random_plane();
if($planeToDisplay == "planechase/spatial-merging.jpg"){
  $planeOne = random_plane();
  $planeTwo = random_plane();
  
  while(in_array($planeOne, $phenoms)){
    $planeOne = random_plane();
  }
  while(in_array($planeTwo, $phenoms)){
    $planeTwo = random_plane();
  }
  echo "<img src='";
  echo $planeToDisplay;
  echo "' width='300' height='210'/> <br>";
  echo "<b>DOUBLE PLANESWALKING TO:</b><br>";
  echo "<img src='";
  echo $planeOne;
  echo "' width='500' height='350'/>";
  echo "<img src='";
  echo $planeTwo;
  echo "' width='500' height='350'/>";
} else { 
  // echo "<img src='";
  //echo $planeToDisplay;
  //echo "' width='500' height='350'/>";
  display_plane($planeToDisplay);

}
?>
 </p>
<table align="center" cellspacing='20'>
<tr>
   <td>
   <form action="index.php" >
   <input type="submit" value="Planeswalk!" />
   </td>
   <td>
<script type="text/javascript">
function planarDie()
{
  var dieRoll = Math.floor((Math.random()*6)+1);
  if(dieRoll == 1){
    alert("PLANESWALKING!");
    self.location='index.php';
  } else if (dieRoll == 6){
    alert("CHAOS EFFECT!");
  } else {
    alert("Nothing Happened...");
  }
}
</script>
<form>
<button type='button' onClick="planarDie()">Roll Die</button>
</form>
</td>
</tr>
</table>

<br><br><br>
<font size='2'>  <a href="http://www.wizards.com/magic/magazine/article.aspx?x=mtg/daily/feature/51">Never played Planechase?</a> - (Custom help page coming soon)</font> <br>
  <font size='1' color='#A8A8A8'> By Ryan Semple. - <a href="https://bitbucket.org/FailCone">bitbucket(github)</a>
<br>
Information presented on this page about Magic: The Gathering is copyrighted by Wizards of the Coast.
<br>
  This website is not endorsed, supported, or affiliated with Wizards of the Coast.
<br>
  Images found at <a href='http://magiccards.info'>magiccards.info</a> -
<a href='http://magiccards.info/extra/phenomenon/planechase-2012-edition/chaotic-aether.html'>2012 Phenomena</a> - 

<a href='http://magiccards.info/extra/plane/planechase-2012-edition/akoum.html'>2012 Planes</a> - 
<a href='http://magiccards.info/extra/plane/planechase/academy-at-tolaria-west.html'>2009 Planes</a>
</font>
<br>
<!-- begin hit counter code --><img style="border: 0px solid ; display: inline;" alt="hit counter" 
  src="http://hit-counter.info/hit.php?id=472547&counter=9"></a> <font size='3'>planes visited!</font>

</body>
</html>